package me.DoppelRR.Game500.EventHandlers;

import java.util.List;

import me.DoppelRR.Game500.Arena;
import me.DoppelRR.Game500.Game500;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

public class SnowballHitSomethingEvent implements Listener {
	
	private Game500 plugin;
	public SnowballHitSomethingEvent(Game500 plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onHit(ProjectileHitEvent e) {
		
		if (!(e.getEntity() instanceof Snowball))
			return;
		
		Snowball oldSB = (Snowball) e.getEntity();
		if (!(oldSB.getShooter() instanceof Player))
			return;
		
		Player shooter = (Player) oldSB.getShooter();
		if (!plugin.playersToGames.containsKey(shooter))
			return;
		
		Arena a = plugin.playersToGames.get(shooter);
		if (!a.getThrower().equals(shooter))
			return;
		
		List<Entity> nearEnts = oldSB.getNearbyEntities(1, 1, 1);
		if (!nearEnts.isEmpty()) {
			if (nearEnts.get(0) instanceof Player) {
				Player catcher = (Player) nearEnts.get(0);
				if (!catcher.equals(a.getThrower())) {
					if (a.isBallAlive() == a.isAlive()) {
						a.setPoints(catcher, a.getPoints(catcher) + a.getPointsToWin());
						int points = a.getPoints(catcher);
						a.broadcast("[500] " + catcher.getName() + " catched the ball. He now has " + points + " points.");
						
						if (points >= 500) {
							a.setPoints(catcher, 0);
							a.setThrower(catcher);
							a.broadcast("[500] Since he has reached 500 points, he is the new thrower.");
						}
						
						a.getThrower().getInventory().addItem(new ItemStack(Material.SNOW_BALL));
						return;
					}
				}
			}
		}
		
		a.setBallAlive(false);
		
		Vector oldVector = oldSB.getVelocity();
		Location hitLoc = oldSB.getLocation();
		
		BlockIterator b = new BlockIterator(hitLoc.getWorld(), 
				hitLoc.toVector(), oldVector, 0, 3);
		
		Block hitBlock = oldSB.getLocation().getBlock();
		
		Block blockBefore = hitBlock;
		Block nextBlock = b.next();
		
		while (b.hasNext() && nextBlock.getType() == Material.AIR) {
			blockBefore = nextBlock;
			nextBlock = b.next();
		}
		
		BlockFace blockFace = nextBlock.getFace(blockBefore);

		if (blockFace != null) {
			
			// Convert blockFace SELF to UP:
			if (blockFace == BlockFace.SELF) {
				blockFace = BlockFace.UP;
			}
			
			Vector hitPlain = new Vector(blockFace.getModX(), blockFace.getModY(), blockFace.getModZ());
			
			double dotProduct = oldVector.dot(hitPlain);
			Vector u = hitPlain.multiply(dotProduct).multiply(2.0);
			
			Snowball newSB = oldSB.getWorld().spawn(hitLoc, Snowball.class);
			newSB.setVelocity(oldVector.subtract(u).multiply(0.5));
			newSB.setShooter(shooter);
			
			oldSB.remove();
		}
	}
}
