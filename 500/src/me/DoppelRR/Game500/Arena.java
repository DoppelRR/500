package me.DoppelRR.Game500;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Arena {
	
	private HashMap<Player, Integer> players = new HashMap<Player, Integer>();
	private Player thrower;
	private String name;
	private Location start;
	private boolean running;
	
	//Settings for each subround
	private int pointsToWin;
	private boolean alive;
	private boolean ballAlive;
	
	public Arena(String name, Location loc) {
		this.name = name;
		this.start = loc;
	}
	
	public Player getThrower() {
		return thrower;
	}
	
	public void setThrower(Player p) {
		thrower = p;
	}

	public HashMap<Player, Integer> getPlayers() {
		return players;
	}

	public String getName() {
		return name;
	}

	public void addPlayer(Player p) {
		players.put(p, 0);
	}
	
	public void removePlayer(Player p) {
		players.remove(p);
	}

	public boolean isRunning() {
		return running;
	}


	public void setRunning(boolean running) {
		this.running = running;
	}

	public Location getStart() {
		return start;
	}
	
	public void setPoints(Player p, int points) {
		players.put(p, points);
	}
	
	public int getPoints(Player p) {
		return players.get(p);
	}
	
	public void newRound() {
		
		Random random = new Random();
		List<Player> keys = new ArrayList<Player>(players.keySet());
		this.setThrower(keys.get(random.nextInt(keys.size())));
		
		alive = random.nextBoolean();
		pointsToWin = (random.nextInt(5) + 1) * 100;
		setBallAlive(true);
		
		thrower.getInventory().addItem(new ItemStack(Material.SNOW_BALL));
		players.put(thrower, 0);
		
		broadcast("The game started, " + thrower.getName() + " is the thrower.");
		if (alive) {
			broadcast("Alive " + pointsToWin);
		} else {
			broadcast("Dead " + pointsToWin);
		}
	}

	public boolean isBallAlive() {
		return ballAlive;
	}

	public void setBallAlive(boolean ballAlive) {
		this.ballAlive = ballAlive;
	}

	public int getPointsToWin() {
		return pointsToWin;
	}

	public void setPointsToWin(int pointsToWin) {
		this.pointsToWin = pointsToWin;
	}
	
	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public void broadcast(String mes) {
		for (Player p : players.keySet())
			p.sendMessage(mes);
	}
	
	
}
