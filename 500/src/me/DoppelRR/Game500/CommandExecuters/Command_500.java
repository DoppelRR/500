package me.DoppelRR.Game500.CommandExecuters;

import me.DoppelRR.Game500.ConfigAccessor;
import me.DoppelRR.Game500.Arena;
import me.DoppelRR.Game500.Game500;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class Command_500 implements CommandExecutor {
	
	private Game500 plugin;
	public Command_500(Game500 plugin) {
		this.plugin = plugin;
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		
		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("leave")) {
				
				if (!(sender instanceof Player)) {
					System.out.println("Only players can leave a arena.");
					return true;
				}
				
				Player p = (Player) sender;
				
				if (!plugin.playersToGames.containsKey(p)) {
					p.sendMessage("You are not in a arena.");
					return true;
				}
				
				Arena a = plugin.playersToGames.get(p);
				a.removePlayer(p);
				plugin.playersToGames.remove(p);
				p.teleport(plugin.spawn);
				
				a.broadcast("[500] " + p.getName() + " left the arena.");
				
				return true;
			}
		} else if (args.length == 2) {
			if (args[0].equalsIgnoreCase("createArena")) {
				
				if (sender instanceof Player) {
					
					Player p = (Player) sender;
					
					ConfigAccessor arenas = new ConfigAccessor(plugin, "Arenas.yml");
					FileConfiguration arenasCfg = arenas.getConfig();
					
					if (arenasCfg.contains(args[1])) {
						p.sendMessage("A arena with this name already exists. Please choose another one.");
						return true;
					}
					
					Location loc = p.getLocation();
					arenasCfg.set(args[1] + ".World", p.getWorld().getName());
					arenasCfg.set(args[1] + ".Pos.X", loc.getBlockX());
					arenasCfg.set(args[1] + ".Pos.Y", loc.getBlockY());
					arenasCfg.set(args[1] + ".Pos.Z", loc.getBlockZ());
					arenas.saveConfig();
					p.sendMessage("Arena " + args[1] + " was created.");
					return true;
				} else {
					System.out.println("Only players can use this command.");
					return true;
				}
				
			} else if (args[0].equalsIgnoreCase("join")) {
				
				if (!(sender instanceof Player)) {
					System.out.println("Only players can join a arena.");
					return true;
				}
				
				Player p = (Player) sender;
				
				if (plugin.playersToGames.containsKey(p)) {
					p.sendMessage("You are in a arena already. Please leave it before joining a new one using /500 leave");
					return true;
				}
				
				if (!plugin.arenaList.containsKey(args[1])) {
					p.sendMessage("This arena does not exist");
					return true;
				}
				
				Arena a = plugin.arenaList.get(args[1]);
				
				if (a.isRunning()) {
					p.sendMessage("This game is already running. Please wait until it is finished.");
					return true;
				}
				
				a.addPlayer(p);
				plugin.playersToGames.put(p, a);
				p.teleport(a.getStart());
				a.broadcast("[500] " + p.getName() + " joined the arena.");
				return true;
			}
		}
		return false;
	}
	
}
