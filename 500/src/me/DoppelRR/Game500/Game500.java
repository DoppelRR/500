package me.DoppelRR.Game500;

import java.util.HashMap;
import java.util.Set;

import me.DoppelRR.Game500.CommandExecuters.Command_500;
import me.DoppelRR.Game500.EventHandlers.SnowballHitSomethingEvent;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Game500 extends JavaPlugin {
	
	public HashMap<String, Arena> arenaList = new HashMap<String, Arena>();
	public HashMap<Player, Arena> playersToGames = new HashMap<Player, Arena>();
	public Location spawn;
	
	@Override
	public void onEnable() {
		
		//load the config
		ConfigAccessor config = new ConfigAccessor(this, "config.yml");
		config.loadConfig();
		
		spawn = new Location(Bukkit.getWorld(config.getConfig().getString("Spawn.World")), config.getConfig().getDouble("Spawn.X"), 
					config.getConfig().getDouble("Spawn.Y"), config.getConfig().getDouble("Spawn.Z"));
		
		//load all Arenas
		ConfigAccessor arenas = new ConfigAccessor(this, "Arenas.yml");
		FileConfiguration arenasCfg = arenas.getConfig();
		
		for (String s : arenasCfg.getKeys(false)) {
			
			final Arena a = new Arena(s, new Location(Bukkit.getWorld(arenasCfg.getString(s + ".World")),
					arenasCfg.getDouble(s + ".Pos.X"), 
					arenasCfg.getDouble(s + ".Pos.Y"), arenasCfg.getDouble(s + ".Pos.Z")));
			
			arenaList.put(s, a);
			
			this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
				
				@Override
				public void run() {
					Set<Player> players = a.getPlayers().keySet();
					
					if (players.size() < 2) {
						if (a.isRunning()) {
							
							a.broadcast("[500] The game stopped because there are not enough players.");
							a.broadcast("[500] But do not worry, you will keep your points if you do not leave.");
							
							//GAME HAS TO STOP
							a.setRunning(false);
							return;
						} else {
							return;
						}
					} else {
						if (a.isRunning())
							return;
						
						a.newRound();
						
						a.setRunning(true);
					}
				}
				
			}, 0L, 1L);
			
			System.out.println("[500] Arena " + s + " has been loaded.");
		}
		
		this.getServer().getPluginManager().registerEvents(new SnowballHitSomethingEvent(this), this);
		this.getCommand("500").setExecutor(new Command_500(this));
		
		System.out.println("[500] was successfully enabled");
	}
	
	@Override
	public void onDisable() {
		System.out.println("[500] was successfully disabled");
	}
	
}
